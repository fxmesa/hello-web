package com.example.helloweb;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;

@Controller
public class HomePageController {

    @RequestMapping("/")
    public String index(Model model) {
        model.addAttribute("now", (new Date()).toString());

        return "layout/application";
    }

    @RequestMapping("/one")
    public String one(Model model) {
        model.addAttribute("page", "1 (one)");

        return "one";
    }

    @RequestMapping("/two")
    public String two(Model model) {
        model.addAttribute("page", "2 (two)");

        return "one";
    }

}

