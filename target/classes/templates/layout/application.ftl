<#macro layout title>
    <html>
    <head>
        <link rel="stylesheet" href="/webjars/bootstrap/3.2.0/css/bootstrap.min.css" />
        <title>${title}</title>
    </head>
    <body>
    <nav class="navbar navbar-default">

        <div class="container">
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="/one/">go to 1</a></li>
                    <li><a href="/two/">go to 2</a></li>
                </ul>
            </div>
        </div>
    </nav>
    <div class="container">
        <h1>Example</h1>
        <#nested>
    </div>
    </body>
    </html>
</#macro>
